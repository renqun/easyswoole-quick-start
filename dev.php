<?php
return [
    'SERVER_NAME' => "EasySwoole",
    'MAIN_SERVER' => [
        'LISTEN_ADDRESS' => '0.0.0.0',
        'PORT' => 9501,
        'SERVER_TYPE' => EASYSWOOLE_WEB_SOCKET_SERVER, //可选为 EASYSWOOLE_SERVER  EASYSWOOLE_WEB_SERVER EASYSWOOLE_WEB_SOCKET_SERVER
        'SOCK_TYPE' => SWOOLE_TCP,
        'RUN_MODEL' => SWOOLE_PROCESS,
        'SETTING' => [
            'worker_num' => 8,
            'reload_async' => true,
            'max_wait_time'=>3
        ],
        'TASK'=>[
            'workerNum'=>4,
            'maxRunningNum'=>128,
            'timeout'=>15
        ]
    ],
    'TEMP_DIR' => '/Temp',
    'LOG_DIR' => '/Log',
    'MYSQL' => [
        'host'     => '39.107.239.207',
        'database' => 'turntable',
        'user'     => 'turntable',
        'password' => 'yLybK3HfT4Pmkwaj',
        'port'     => 3306,
        'timeout'  => 15,
    ],
    'ELASTICSEARCH' => [
        'host'     => '127.0.0.1',
        'port'     => 9200,
        // 'username' => 'root',
        // 'password' => '123456'
    ],
    'JWT' => [
        'key' => 'SDOXJJ^124&784XD!MC~@#1XS@',
    ],
    'REDIS' => [
        'host'      => '39.107.239.207',
        'port'      => 6379,
        'auth'      => '5QXce2vh0a',
        'db'        => 6,
        'serialize' => \EasySwoole\Redis\Config\RedisConfig::SERIALIZE_NONE
    ]
];
