<?php

namespace App\Util\Log;

use EasySwoole\Log\LoggerInterface;
use EasySwoole\EasySwoole\Config;

class QueueLog implements LoggerInterface
{

    use \EasySwoole\Component\Singleton;

    private $queue;

    private function __construct($queue)
    {
        $this->queue   = $queue;
        $this->logDir  = getcwd();
        $this->logDir  = $this->logDir . Config::getInstance()->getConf("LOG_DIR");
    }

    public function info($msg)
    {
        $this->console($msg, self::LOG_LEVEL_INFO);
        $this->log($msg, self::LOG_LEVEL_INFO);
    }

    public function error(string $msg)
    {
        $this->console($msg, self::LOG_LEVEL_ERROR);
        $this->log($msg, self::LOG_LEVEL_ERROR);
    }

    public function log(?string $msg, int $level = self::LOG_LEVEL_INFO, string $category = 'DEBUG')
    {
        $date = date('Y-m-d H:i:s');
        $levelStr = $this->levelMap($level);
        $filePath = $this->logDir . "/queue.log";
        // $filePath = $this->logDir . "/" . date('Y-m-d') . "-queue.log";
        $str = "[{$date}][{$levelStr}] : {$msg}\n";
        file_put_contents($filePath, $str, FILE_APPEND | LOCK_EX);
        return $str;
    }

    public function console(?string $msg, int $level = self::LOG_LEVEL_INFO, string $category = 'DEBUG')
    {
        $date = date('Y-m-d H:i:s');
        $temp = $this->colorString("[{$date}] [{$this->queue}]: {$msg}", $level) . "\n";
        fwrite(STDOUT, $temp);
    }


    private function colorString(string $str, int $level)
    {
        switch ($level) {
            case self::LOG_LEVEL_INFO:
                $out = "[42m";
                break;
            case self::LOG_LEVEL_ERROR:
                $out = "[41m";
                break;
            default:
                $out = "[42m";
                break;
        }
        return chr(27) . "$out" . "{$str}" . chr(27) . "[0m";
    }

    private function levelMap(int $level)
    {
        switch ($level) {
            case self::LOG_LEVEL_INFO:
                return 'INFO';
            case self::LOG_LEVEL_ERROR:
                return 'ERROR';
            default:
                return 'UNKNOWN';
        }
    }
}
