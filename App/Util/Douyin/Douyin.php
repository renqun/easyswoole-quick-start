<?php

namespace App\Util\Douyin;

use EasySwoole\Redis\Redis;
use EasySwoole\RedisPool\RedisPool;
use EasySwoole\EasySwoole\Config;
use EasySwoole\HttpClient\HttpClient;

class Douyin
{

    /**
     * 抖音应用 配置信息
     */
    protected $config;

    protected $accessToken;
    protected $refreshToken;
    protected $openId;



    public function __construct()
    {
        $this->config = Config::getInstance()->getConf("DOUYIN");
        return $this;
    }

    public function getAccessToken($code)
    {
        $config = $this->config;
        $client = new HttpClient("https://open.douyin.com/oauth/access_token/");
        $client->setQuery([
            'client_key'    => $config['client_key'],
            'client_secret' => $config['client_secret'],
            'code'          => $code,
            'grant_type'    => 'authorization_code',
        ]);
        $client->setEnableSSL(true);
        $res = $client->get();
        var_dump($res);
        // $openid = 
        // $accessToken = 
        // RedisPool::invoke(function (Redis $redis) use () {
        //     return $redis->set("_Douyin:access_token:{$openid}", $accessToken);
        // });

        // $accessToken = RedisPool::invoke(function (Redis $redis) use ($config) {
        //     return $redis->get("_Douyin:access_token:{$config['client_key']}");
        // });
        // $accessToken = RedisPool::invoke(function (Redis $redis) use ($config) {
        //     return $redis->get("_Douyin:access_token:{$config['client_key']}");
        // });
        // if(!empty($accessToken)) {
        //     $this->accessToken = $accessToken;
        // }else {
        //     $client = new HttpClient("https://open.douyin.com/oauth/access_token/");
        //     $client->setQuery([
        //         'client_key'    => $config['client_key'],
        //         'client_secret' => $config['client_secret'],
        //         'code'          => $code,
        //         'grant_type'    => 'authorization_code',
        //     ]);
        //     $client->setEnableSSL(true);
        //     $res = $client->get();
        //     var_dump($res);
        // }
    }

    /**
     * 获取 RefreshToken
     */
    public function getRefreshToken($openid)
    {
        $refreshToken = RedisPool::invoke(function (Redis $redis) use ($openid) {
            return $redis->get("_Douyin:refresh_token:{$openid}");
        });

        if (empty($refreshToken)) {
            throw new \Exception('请重新授权');
            // $refreshToken = $this->refreshRefreshToken();
        }
        return $refreshToken;
    }

    
    /**
     * 根据
     */
    public function getAccessTokenByOpenid($openid)
    {
        $accessToken = RedisPool::invoke(function (Redis $redis) use ($openid) {
            return $redis->get("_Douyin:access_token:{$openid}");
        });

        if(empty($accessToken)) {
            $accessToken = $this->refreshAccessToken($openid);
        }
        return $accessToken;
    }


    /**
     * 刷新 token
     */
    public function refreshAccessToken($openid)
    {
        $refreshToken = $this->getRefreshToken($openid);
        $config = $this->config;
        $client = new HttpClient("https://open.douyin.com/oauth/refresh_token/");
        $client->setQuery([
            'client_key'    => $config['client_key'],
            'refresh_token' => $refreshToken,
            'grant_type'    => 'refresh_token',
        ]);
        $client->setEnableSSL(true);
        $res = $client->get();
        var_dump($res);

    }



    public function openId()
    {

    }

    /**
     * @var $code 用户code
     */
    public function oauthUserInfo($code)
    {
        $this->accessToken = $this->getAccessToken($code);
        $openid       = $this->openid;
        $accessToken  = $this->accessToken;
        go(function () use ($openid, $accessToken) {
            try {
                $client = new HttpClient("https://open.douyin.com/oauth/userinfo/");
                $client->setQuery([
                    'open_id'      => $openid,
                    'access_token' => $accessToken,
                ]);
                $client->setEnableSSL(true);
                $res = $client->get();
                var_dump($res);
            } catch (\Exception $e) {
                // throw new \Exception($e->getMessage(), $e->getCode());
            }
        });
    }


    /**
     * 校验 $floower是否关注了 $openid
     */
    public function fansCheck($floower, $openid)
    {
        $accessToken = $this->getAccessTokenByOpenid($openid);
        go(function () use ($floower, $openid, $accessToken) {
            try {
                $client = new HttpClient("https://open.douyin.com/fans/check/");
                $client->setQuery([
                    'open_id'          => $openid,
                    'follower_open_id' => $floower,
                    'access_token'     => $accessToken, // 该 token 为 被关注者 openid 的 accesstoken
                ]);
                $client->setEnableSSL(true);
                $res = $client->get();
                var_dump($res);
            } catch (\Exception $e) {
                // throw new \Exception($e->getMessage(), $e->getCode());
            }
        });
    }

}
