<?php

namespace App\Util\Redis;

use EasySwoole\RedisPool\RedisPool as Pool;
use EasySwoole\Redis\Config\RedisConfig;
use EasySwoole\EasySwoole\Config;

class RedisPool
{

    public function __construct()
    {
        $config = Pool::getInstance()->register(new RedisConfig(Config::getInstance()->getConf("REDIS")));
        $config->setMinObjectNum(5);
        $config->setMaxObjectNum(20);
    }

}
