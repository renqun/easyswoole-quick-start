<?php

namespace App\Util\ElasticSearch;


use EasySwoole\ElasticSearch\RequestBean\Delete;
use EasySwoole\ElasticSearch\RequestBean\Create;
use EasySwoole\ElasticSearch\RequestBean\Get;
use EasySwoole\ElasticSearch\RequestBean\Mget;
use EasySwoole\ElasticSearch\ElasticSearch;
use EasySwoole\ElasticSearch\Config as EsConfig;
use EasySwoole\EasySwoole\Config;

class Es
{

    protected $es;
    protected $defaultType = 'default';

    public function __construct()
    {
        $config = new EsConfig(Config::getInstance()->getConf("ELASTICSEARCH"));
        return $this->es = new ElasticSearch($config);
    }


    /**
     * @var $index 索引
     */
    public function create($index, $id, $data)
    {
        $es   = $this->es;
        $type = $this->defaultType;
        go(function() use($es, $type, $index, $id, $data) {
            $bean = new Create();
            $bean->setIndex($index);
            $bean->setId($id);
            $bean->setType($type);
            $bean->setBody($data);
            return $es->client()->create($bean)->getBody();
        });
        
    }

    /**
     * @var $index 索引
     * @var $id    文档主键
     */
    public function get($index, $id)
    {
        $es   = $this->es;
        $type = $this->defaultType;
        go(function() use($es, $type, $index, $id) {
            $bean = new Get();
            $bean->setIndex($index);
            $bean->setType($type);
            $bean->setId($id);
            $response = $es->client()->get($bean)->getBody();
            return json_decode($response, true);
        });
    }

    /**
     * @var $index 索引
     * @var $ids   文档主键集合
     */
    public function all($index, $ids)
    {
        $es   = $this->es;
        $type = $this->defaultType;
        go(function() use($es, $type, $index, $ids) {
            $bean = new Mget();
            $bean->setIndex($index);
            $bean->setType($type);
            $bean->setBody(['ids' => $ids]);
            $response = $es->client()->mget($bean)->getBody();
            return json_decode($response, true);
        });
    }

    /**
     * @var $index 索引
     * @var $id    文档主键
     */
    public function del($index, $id)
    {
        $es = $this->es;
        go(function() use ($es, $index, $id) {
            $bean = new Delete();
            $bean->setIndex($index);
            $bean->setId($id);
            $response = $es->client()->delete($bean)->getBody();
            return json_decode($response, true);
        });
        
    }
}
