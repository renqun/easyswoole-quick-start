<?php

namespace App\Models\TurnTable;

use App\Models\Common\BaseModel;
use EasySwoole\ORM\Utility\Schema\Table;

/**
 * 转盘模型
 * Class User
 */
class TurnTable extends BaseModel
{

    
    protected $tableName = 'turn_table';


    protected $autoTimeStamp = 'datetime';
    protected $createTime    = 'create_time';
    protected $updateTime    = 'update_time';


    protected $casts = [
        
    ];


    /**
     * 表的获取
     * 此处需要返回一个 EasySwoole\ORM\Utility\Schema\Table
     * @return Table
     */
    // public function schemaInfo(bool $isCache = true): Table
    // {
    //     $table = new Table($this->tableName);
    //     $table->colInt('id')->setIsPrimaryKey(true);
    //     $table->colChar('name', 255);
    //     $table->colInt('age');
    //     return $table;
    // }

}