<?php

namespace App\Crontab\Task;

use EasySwoole\EasySwoole\Crontab\AbstractCronTask;
use EasySwoole\EasySwoole\Task\TaskManager;

class CustomTask extends AbstractCronTask
{

    // 定时任务的名称
    protected $taskName = 'CustomCrontab';

    // 运行规则
    protected $rule = '*/1 * * * *'; # 每天 `00:15` 执行任务



    public function run(int $taskId, int $workerIndex)
    {
        // 定时任务的执行逻辑
        echo '测试定时任务执行在' . date('Y-m-d H:i:s') . "\n";
        // 开发者可投递给task异步处理
        TaskManager::getInstance()->async(function (){
            
        });
    }


    public static function getRule(): string
    {
        return $this->rule;
    }


    public static function getTaskName(): string
    {
        return $this->taskName;
    }


    public function onException(\Throwable $throwable, int $taskId, int $workerIndex)
    {
        // 捕获run方法内所抛出的异常
    }
}