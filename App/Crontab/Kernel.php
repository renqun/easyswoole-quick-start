<?php

namespace App\Crontab;

use App\Crontab\Task\CustomTask;
use EasySwoole\EasySwoole\Crontab\Crontab;


class Kernel
{
    /**
     * 注册定时
     */
    public function __construct() 
    {
        Crontab::getInstance()->addTask(CustomTask::class); // 开始一个定时任务计划     
    }

}