<?php

namespace App\Languages;

use EasySwoole\I18N\AbstractDictionary;


class Dictionary extends AbstractDictionary
{
    
    const Success            = 'Success';

    const Action_Not_Found   = 'Action_Not_Found';


    const User_Need_Login    = 'User_Not_Found';     // 请重新登录
    const User_Not_Found     = 'User_Not_Found';     // 用户不存在
    const User_Token_Error   = 'User_Token_Error';   // 用户身份认证错误
    const User_Token_Expired = 'User_Token_Expired'; // 用户身份认证已过期
    const Auth_Error         = 'Auth_Error';         // 无操作权限

    
}