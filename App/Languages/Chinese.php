<?php

namespace App\Languages;

// 定义一个中文语言包
class Chinese extends Dictionary
{
    
    const Success            = '成功';

    const Action_Not_Found   = '此 Action 不存在';

    const User_Need_Login    = '请重新登录';
    const User_Not_Found     = '用户不存在';
    const User_Token_Error   = '用户身份认证错误';
    const User_Token_Expired = '用户身份认证已过期';
    const Auth_Error         = '您无此操作权限';

}