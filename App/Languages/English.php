<?php

namespace App\Languages;

// 定义一个英文语言包
class English extends Dictionary
{

    const Success            = 'Success';

    const Action_Not_Found   = 'Action not found';


    const User_Need_Login    = 'You need login again';
    const User_Not_Found     = 'User not found';
    const User_Token_Error   = 'The token is error';
    const User_Token_Expired = 'The token has expired';
    const Auth_Error         = 'You do not have this permission';
    
}