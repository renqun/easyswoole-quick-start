<?php

namespace App\HttpController;

use App\Exception\Status;
use App\Exception\StatusMessage;
use App\Exception\BaseException;
use App\Services\User\User as UserService;
use EasySwoole\Component\Context\ContextManager;
use EasySwoole\HttpAnnotation\AnnotationController;
use EasySwoole\HttpAnnotation\Exception\Annotation\ParamValidateError;

class BaseController extends AnnotationController
{

    /**
     * 用户信息
     */
    public $userInfo = [];


    protected function onRequest(?string $action): ?bool
    {
        $token = $this->request()->getHeader('authorization');
        if(empty($token)) return true;
        // 解密 token
        $this->userInfo = UserService::getUserInfoByToken($token[0]);
        return true;
    }

    /**
     * 校验用户权限
     */
    public function checkAuth($auth = null)
    {
        if(empty($this->userInfo)) throw new BaseException(StatusMessage::You_Need_Login, Status::You_Need_Login);
        // 开始校验权限
        if(!empty($auth) && false) {
            throw new BaseException(StatusMessage::Auth_Error, Status::Auth_Error);
        }
    }

    public function userInfo($key = 'all')
    {
        if($key == 'all') return $this->userInfo;
        return $this->userInfo[$key];
    }

    /**
     * 从请求中获取参数
     */
    public function params()
    {
        return $this->request()->getRequestParam();
    }

    /**
     * 从上下文中获取数据
     */
    public function getDataByContext($key)
    {
        return ContextManager::getInstance()->get($key);
    }

    /**
     * 验证数据
     */
    public function validate($data, $rules, $messages = [], $alias = []) 
    {
        $validate = \EasySwoole\Validate\Validate::make($rules, $messages, $alias);
        if (!$validate->validate($data)) throw new ParamValidateError($validate->getError()->__toString());
        return $data;
    }


    public function success($data, $message = 'Success')
    {
        $this->response()->write(json_encode([
            'status'  => 'ok',
            'code'    => 200,
            'data'    => $data,
            'message' => $message,
        ]));

    }
 
    public function fail($code, $message, $data = [])
    {
        $this->response()->write(json_encode([
            'status'  => false,
            'code'    => $code,
            'data'    => $data,
            'message' => $message,
        ]));
    }

}