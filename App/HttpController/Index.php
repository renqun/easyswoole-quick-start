<?php


namespace App\HttpController;

use App\Exception\Status;
use App\Exception\StatusMessage;
use App\Queue\CancelOrderQueue;
use App\Services\User\Login as LoginService;
use EasySwoole\HttpAnnotation\AnnotationTag\Param;
use EasySwoole\HttpAnnotation\AnnotationTag\Api;
use EasySwoole\HttpAnnotation\AnnotationTag\ApiDescription;
use EasySwoole\HttpAnnotation\AnnotationTag\Method;
use EasySwoole\HttpAnnotation\AnnotationController;
use EasySwoole\HttpAnnotation\Exception\Annotation\ParamValidateError;

class Index extends AnnotationController
{

    /**
     * @Api(name="test",path="/test",version="1.0")
     * @Param(name="type",required="")
     * @ApiDescription("测试接口")
     * @Method(allow={GET,POST})
     */
    public function test()
    {
        $this->response()->write(json_encode([
            'status'  => 'ok',
            'code'    => 200,
            'data'    => uuid(),
            'message' => 'success',
        ]));
        return;
        CancelOrderQueue::dispatch([
            'uuid' => 'abcdef',
        ]);
        $res = [];
        $this->response()->write(json_encode([
            'status'  => 'ok',
            'code'    => 200,
            'data'    => $res,
            'message' => 'success',
        ]));
        
    }

    protected function actionNotFound(?string $action)
    {
        $this->response()->write(json_encode([
            'status'  => false,
            'code'    => Status::Action_Not_Found,
            'message' => StatusMessage::Action_Not_Found,
        ]));
    }
}