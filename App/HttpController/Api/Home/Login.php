<?php


namespace App\HttpController\Api\Home;

use App\HttpController\BaseController;
use App\Services\User\Login as LoginService;
use EasySwoole\HttpAnnotation\AnnotationTag\Param;
use EasySwoole\HttpAnnotation\AnnotationTag\Api;
use EasySwoole\HttpAnnotation\AnnotationTag\ApiDescription;
use EasySwoole\HttpAnnotation\AnnotationTag\Method;

class Login extends BaseController
{

    /**
     * @Api(name="home.getLoginVerifyCode",path="/api/home/login/code",version="1.0")
     * @Param(name="type",defaultValue="img")
     * @ApiDescription("获取登录凭证码, 形式可以为sms手机验证码, img图形码, qr二维码")
     * @Method(allow={GET})
     */
    public function verifyCode($type)
    {
        switch ($type) {
            case 'img':
                $config = new \EasySwoole\VerifyCode\Conf();
                $config->setCharset('234578AaBbCcDdEeFfGgHhKkMmNnPpQqRrSsTtUuVvWwXxYy');
                $code = new \EasySwoole\VerifyCode\VerifyCode($config);
                // 生成验证码
                $code = $code->DrawCode();
                // 获取生成的验证码内容字符串 string(4) "0rnh"
                // 可存储起来和用户输入的验证码比对
                $codeStr = $code->getImageCode();

                $this->response()->withHeader('Content-Type', 'image/png');
                $this->response()->write($code->getImageByte());
                return;
            case 'sms':
                $mobile = $this->request()->getRequestParam('mobile');
                break;
        }
        
    }


    /**
     * @Api(name="home.login",path="/api/home/login",version="1.0")
     * @Param(name="type",defaultValue="accoount")
     * @ApiDescription("统一登录接口, 形式可以为accoount密码登录")
     * @Method(allow={POST})
     */
    public function login($type)
    {
        switch ($type) {
            case 'accoount':
                $params = $this->validate($this->params(),[
                    'account'  => 'required',
                    'password' => 'required',
                ]);
                $this->success(LoginService::loginHomeByAccount($params));
                break;
        }
        
    }
    
}
