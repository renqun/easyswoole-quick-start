<?php


namespace App\HttpController\Api\Home;

use EasySwoole\Http\Request;
use App\HttpController\BaseController;
use EasySwoole\HttpAnnotation\AnnotationTag\Param;
use EasySwoole\HttpAnnotation\AnnotationTag\Api;
use EasySwoole\HttpAnnotation\AnnotationTag\ApiDescription;
use EasySwoole\HttpAnnotation\AnnotationTag\Method;

class User extends BaseController
{


    /**
     * @Api(name="home.getMineInfo",path="/api/home/user/info",version="1.0")
     * @ApiDescription("获取用户信息")
     * @Method(allow={GET})
     */
    public function info()
    {
        $this->checkAuth('home.getMineInfo');
        echo 'controller';
    }
    
}
