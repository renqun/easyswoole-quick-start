<?php


namespace App\HttpController\Api\Admin;

use App\Export\OrderExport;
use App\HttpController\BaseController;
use App\Services\User\Login as LoginService;
use EasySwoole\HttpAnnotation\AnnotationTag\Param;
use EasySwoole\HttpAnnotation\AnnotationTag\Api;
use EasySwoole\HttpAnnotation\AnnotationTag\ApiDescription;
use EasySwoole\HttpAnnotation\AnnotationTag\Method;

class User extends BaseController
{

    /**
     * @Api(name="admin.getUserInfo",path="/api/admin/user/info/{uuid}",version="1.0")
     * @ApiDescription("获取用户信息")
     * @Method(allow={GET})
     */
    public function info()
    {
        
    }

    /**
     * @Api(name="admin.downloadUserList",path="/api/admin/user/download",version="1.0")
     * @ApiDescription("后台下载用户列表")
     * @Method(allow={GET})
     */
    public function download()
    {
        try {
            $excel = new OrderExport($this->response());
            $excel->outPut('测试导出', [
                [
                    'id' => 1,
                ]
            ], ['id' => 'ID']);
        } catch (\Exception $e) {
            
        }


    }

}
