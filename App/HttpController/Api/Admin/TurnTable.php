<?php


namespace App\HttpController\Api\Admin;

use App\HttpController\BaseController;
use App\Services\TurnTable\TurnTable as TurnTableService;
use EasySwoole\HttpAnnotation\AnnotationTag\Param;
use EasySwoole\HttpAnnotation\AnnotationTag\Api;
use EasySwoole\HttpAnnotation\AnnotationTag\ApiDescription;
use EasySwoole\HttpAnnotation\AnnotationTag\Method;
use EasySwoole\HttpAnnotation\AnnotationTag\InjectParamsContext;

class TurnTable extends BaseController
{

    /**
     * @Api(name="admin.getTurnTableList",path="/api/admin/turntable/list",version="1.0")
     * @Param(name="page",defaultValue=1)
     * @Param(name="limit",defaultValue=10)
     * @InjectParamsContext(key="params")
     * @ApiDescription("后台获取大转盘列表")
     * @Method(allow={GET})
     */
    public function list()
    {
        // $this->checkAuth('admin.getTurnTableList');
        $params = $this->getDataByContext('params');
        $this->success(TurnTableService::adminGetTurnTableList($params));
    }


    /**
     * @Api(name="admin.createTurnTable",path="/api/admin/turntable/create",version="1.0")
     * @Param(name="sn_number",required="",lengthMax="32")
     * @InjectParamsContext(key="params")
     * @ApiDescription("后台新增大转盘")
     * @Method(allow={POST})
     */
    public function create()
    {
        // $this->checkAuth('admin.createTurnTable');
        $data = $this->validate($this->getDataByContext('params'), [
            'sn_number'  => 'required',
        ]);
        $this->success(TurnTableService::adminCreateTurnTable($data));
    }

}
