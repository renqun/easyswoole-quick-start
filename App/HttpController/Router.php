<?php


namespace App\HttpController;


use EasySwoole\Http\AbstractInterface\AbstractRouter;
use EasySwoole\HttpAnnotation\Utility\Scanner;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use FastRoute\RouteCollector;

class Router extends AbstractRouter
{
    
    function initialize(RouteCollector $router)
    {

        // 全局模式拦截
        $this->setGlobalMode(true);
        $this->setRouterNotFoundCallBack('/actionNotFound');

        $scanner = new Scanner();
        $scanner->mappingRouter($router, EASYSWOOLE_ROOT . '/App/HttpController', 'App\HttpController');



    }
}