<?php

namespace App\Queue;

use App\Util\Log\QueueLog;
use EasySwoole\Component\Singleton;
use EasySwoole\Queue\QueueDriverInterface;
use EasySwoole\Queue\Queue;
use EasySwoole\Queue\Job;

class CancelOrderQueue extends Queue
{

    use Singleton;

    /**
     * 任务被处理的延迟时间（秒）
     */
    public static $delay = 5;

    /**
     * 任务可尝试的次数 任务如果没有确认，则会执行 $tries 次
     */
    public static $tries = 1;


    public function __construct(QueueDriverInterface $driver)
    {
        parent::__construct($driver);
    }

    /**
     * 分发一条 `取消订单` 任务
     */
    public static function dispatch($data)
    {
        $job = new Job();
        $job->setJobData($data);
        $job->setDelayTime(self::$delay);
        // $job->setRetryTimes(self::$tries);
        self::getInstance()->producer()->push($job);
    }


    public static function dispatchNow($data)
    {
        return self::handle($data);
    }

    /**
     * 执行任务
     */
    public static function handle($data)
    {
        try {
            QueueLog::getInstance('CancelOrder')->info("取消订单 start [{$data['uuid']}]");


            QueueLog::getInstance('CancelOrder')->info("取消订单 end   [{$data['uuid']}] 成功");

            return true;
        } catch (\Exception $e) {
            return self::failed($data, $e);
        }
    }


    public static function failed($data, \Exception $e)
    {
        QueueLog::getInstance('CancelOrder')->error("取消订单 [{$data['uuid']}] 失败");
    }
}
