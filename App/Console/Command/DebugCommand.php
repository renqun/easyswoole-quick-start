<?php

namespace App\Console\Command;

use App\Util\ElasticSearch\Es;
use EasySwoole\Command\AbstractInterface\CommandHelpInterface;
use EasySwoole\Command\AbstractInterface\CommandInterface;
use EasySwoole\Command\CommandManager;
use EasySwoole\EasySwoole\Command\Utility;
use App\Util\Douyin\Douyin;

class DebugCommand implements CommandInterface
{
    public function commandName(): string
    {
        return 'debug';
    }

    // 设置自定义命令描述
    public function desc(): string
    {
        return 'debug command';
    }

    public function exec(): ?string
    {

        // 获取文档
        $res = (new Es)->get('test', 1);
        var_dump($res);
        return $res;

        // 删除文档
        $res = (new Es)->del('test', 1);
        var_dump($res);
        return null;
        
        (new Es)->create('test', 1, [
            'a' => 1,
        ]);
        return null;
        $res = (new Douyin())->oauthUserInfo('11111');
        var_dump($res);
        $res = (new Douyin())->fansCheck('11111', '2222');
        return null;
    }

    public function help(CommandHelpInterface $commandHelp): CommandHelpInterface
    {
        return $commandHelp;
    }


}