<?php

namespace App\Console;


class Kernel
{

    /**
     * 注册 命令
     */
    public function __construct() 
    {
        \EasySwoole\Command\CommandManager::getInstance()->addCommand(new \App\Console\Command\TestCommand());
        \EasySwoole\Command\CommandManager::getInstance()->addCommand(new \App\Console\Command\DebugCommand());
    }

}