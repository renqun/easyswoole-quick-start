<?php

namespace App\Export;

use App\Util\Office\Excel;
use App\Util\Office\ExcelSpreadsheet;


class OrderExport
{

    private $response;

    
    public function __construct($response)
    {
        $this->response = $response;
    }


    /**
     * @param string $name 表名称
     * @param array $data  要导出excel表的数据
     * @param array $maps  属性与表头对应关系
     * @throws \Exception
     */
    public function outPut(string $name, array $data, array $maps)
    {
        $head = array_keys($maps);
        $keys = array_values($maps);

        if (count($data) > 2000) {
            throw new \Exception("最多支持导出两千条数据");
        }
        if (count($head) > 26) {
            throw new \Exception("最多支持导出26个字段");
        }
        if (count($keys) > 26) {
            throw new \Exception("最多支持导出26个字段");
        }

        $spreadsheet_logic = new ExcelSpreadsheet($name, $data, $head, $keys);
        $file_name         = $spreadsheet_logic->outPut();

        //生成文件后，使用response输出
        $this->response->write(file_get_contents($file_name));
        $this->response->withHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); //告诉浏览器输出07Excel文件
        $this->response->withHeader('Content-Disposition', 'attachment;filename=' . $file_name); //告诉浏览器输出浏览器名称
        $this->response->withHeader('Cache-Control', 'max-age=0'); //禁止缓存
        $this->response->end();
        unlink($file_name);
    }
}
