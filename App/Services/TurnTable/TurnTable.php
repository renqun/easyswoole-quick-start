<?php

namespace App\Services\TurnTable;

use EasySwoole\Jwt\Jwt;
use App\Exception\BaseException;
use EasySwoole\EasySwoole\Config;
use App\Services\Common\BaseService;
use App\Models\TurnTable\TurnTable as TurnTableModel;


class TurnTable extends BaseService
{

    public static function adminGetTurnTableList($conditions)
    {
        var_dump($conditions);
        $handle = TurnTableModel::create()
            ->limit($conditions['limit'] * ($conditions['page'] - 1), $conditions['limit'])
            ->withTotalCount();
        $list   = $handle->all();
        $count  = $handle->lastQueryResult()->getTotalCount();
        return [
            'list'   => $list,
            'count'  => $count,
        ];
    }


    public static function adminCreateTurnTable($data)
    {
        try {
            self::startTrans();
            $res = TurnTableModel::create()
                ->data([
                    'sn_number' => $data['sn_number'],
                ])
                ->save();
            self::commitTrans();
            return $res;
        } catch (\Exception $e) {
            self::rollbackTrans();
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }
    
    

}
