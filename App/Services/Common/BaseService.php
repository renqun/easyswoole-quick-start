<?php

namespace App\Services\Common;



class BaseService
{
    
    protected static function startTrans()
    {
        \EasySwoole\ORM\DbManager::getInstance()->startTransaction();
    }

    protected static function commitTrans()
    {
        \EasySwoole\ORM\DbManager::getInstance()->commit();
    }

    protected static function rollbackTrans()
    {
        \EasySwoole\ORM\DbManager::getInstance()->rollback();
    }

}
