<?php

namespace App\Services\User;

use App\Exception\Status;
use EasySwoole\Jwt\Jwt;
use App\Exception\BaseException;
use App\Exception\StatusMessage;
use EasySwoole\EasySwoole\Config;
use App\Services\Common\BaseService;
use App\Models\User\User as UserModel;


class User extends BaseService
{

    public static function getUserInfo($conditions)
    {
        $user = UserModel::create()
            ->where('user_id', $conditions['user_id'])
            ->get();
        if(empty($user)) throw new BaseException(StatusMessage::Account_Not_Found, Status::Account_Not_Found);
        return $user;
    }
    
    public static function getUserInfoByToken($token)
    {
        try {
            $jwt = Jwt::getInstance()
                ->setSecretKey(Config::getInstance()->getConf("JWT")['key']) // 秘钥
                ->decode($token);
            switch ($jwt->getStatus()) {
                case  1:
                    $user = UserModel::create()
                        ->where('user_id', $jwt->getAud())
                        ->field(['user_id', 'user_name'])
                        ->get();
                    if (empty($user)) throw new BaseException(StatusMessage::Account_Not_Found, Status::Account_Not_Found);
                    return $user->toArray();
                case  -1:
                    throw new BaseException(StatusMessage::Token_Error, Status::Token_Error);
                case  -2:
                    throw new BaseException(StatusMessage::Token_Expired, Status::Token_Expired);
            }

        } catch (\EasySwoole\Jwt\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }

}
