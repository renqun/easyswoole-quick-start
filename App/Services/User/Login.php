<?php

namespace App\Services\User;

use App\Exception\Status;
use EasySwoole\Jwt\Jwt;
use EasySwoole\EasySwoole\Config;
use App\Exception\BaseException;
use App\Exception\StatusMessage;
use App\Services\Common\BaseService;
use App\Models\User\User as UserModel;

class Login extends BaseService
{   
    
    public static function loginAdminByAccount($account, $password)
    {
        try {
            self::startTrans();
            $res = UserModel::create()
                ->where('account', $account)
                ->get();
            self::commitTrans();
            return true;
        } catch (\Exception $e) {
            self::rollbackTrans();
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }


    public static function loginHomeByAccount($data)
    {
        try {
            self::startTrans();
            $user = UserModel::create()
                ->where('account', $data['account'])
                ->get()
                ->visible(['user_id', 'user_name', 'password'])
                ->toArray();
            if(empty($user)) throw new BaseException(StatusMessage::Account_Not_Found, Status::Account_Not_Found);
            // 原数据库的密码加密使用 laravel 内置库 暂不解决此问题
            unset($user['password']);
            $res = [
                'info'  => $user,
                'token' => self::releaseToken($user),
            ];
            self::commitTrans();
            return $res;
        } catch (\Exception $e) {
            self::rollbackTrans();
            throw new \Exception($e->getMessage(), $e->getCode());
        }
    }
    
    public static function releaseToken($user)
    {
        $jwt = Jwt::getInstance()
            ->setSecretKey(Config::getInstance()->getConf("JWT")['key']) // 秘钥
            ->publish();
        $jwt->setAud($user['user_id']); // 用户
        // $jwt->setExp(time() + 3600); // 过期时间
        $jwt->setIat(time()); // 发布时间
        $jwt->setIss('UserLoginService'); // 发行人
        $jwt->setJti($user['user_id']);
        $jwt->setSub('User');

        // 自定义数据
        $jwt->setData([
            'other_info'
        ]);

        return $jwt->__toString();
    }

}
