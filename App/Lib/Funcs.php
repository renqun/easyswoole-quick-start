<?php

use App\Exception\Status;

/**
 * 生成雪花ID
 */
if (!function_exists("uuid")) {

    function uuid()
    {
        return \EasySwoole\Utility\SnowFlake::make();
    }

}
