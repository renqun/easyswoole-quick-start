<?php

namespace App\Lib;

use EasySwoole\HttpClient\HttpClient;
use EasySwoole\Utility\IntStr;
use EasySwoole\Utility\Str;

class Common
{

    /**
     * @https://www.easyswoole.com/Components/httpClient.html
     */
    public static function request($url, $method = 'GET', $params = [])
    {
        $client = new HttpClient($url);
        $client->setQuery($params); 
        if(Str::startsWith($url, 'https', false)) $client->setEnableSSL(true);
        switch ($method) {
            case 'GET':
                $client->get();
                break;
            case 'POST':
                $client->post();
                break;
        }
    }

}
