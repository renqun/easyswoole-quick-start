<?php
namespace App\Exception;

use App\Exception\Status;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\EasySwoole\Logger;
use App\Exception\BaseException;
use EasySwoole\HttpAnnotation\Exception\Annotation\ParamValidateError;

class Handler
{

    public static function render(\Throwable $e, Request $request, Response $response)
    {
        if($e instanceof ParamValidateError) {
            $errCode    = Status::Validate_Error;
            $errMessage = StatusMessage::Validate_Error;
        }
        else if ($e instanceof BaseException) {
            
            $errCode    = $e->getCode();
            $errMessage = $e->getMessage();
            Logger::getInstance()->error("手动抛出的错误 -- {$errCode} {$errMessage} At '{$e->getFile()}' On {$e->getLine()}\n }", $errCode);
        }
        else {
            $errCode    = $e->getCode();
            $errMessage = $e->getMessage();
            Logger::getInstance()->error("{$errCode} {$errMessage} At '{$e->getFile()}' On {$e->getLine()}\n {$e->getTraceAsString()}", $errCode);
        }
        $response->write(json_encode([
            'status'  => 'error',
            'code'    => $errCode,
            'message' => $errMessage,
        ]));
    }
}