<?php

namespace App\Exception;


class Status
{

    const Success            = 200;
    const Validate_Error     = 2001;
    const Action_Not_Found   = 2002;
    const You_Need_Login     = 1001;
    const Account_Not_Found  = 1002;
    const Token_Error        = 1003;
    const Token_Expired      = 1004;
    const Auth_Error         = 1005;

}
