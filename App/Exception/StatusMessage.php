<?php

namespace App\Exception;


class StatusMessage
{

    const Success            = '成功';
    const Validate_Error     = '验证错误';
    const Action_Not_Found   = '此 Action 不存在';

    const You_Need_Login     = '请重新登录';
    const Account_Not_Found  = '用户不存在';
    const Token_Error        = '用户身份认证错误';
    const Token_Expired      = '用户身份认证已过期';
    const Auth_Error         = '您无此操作权限';

}
