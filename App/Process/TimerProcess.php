<?php
namespace App\Process;

use EasySwoole\Component\Process\AbstractProcess;
use Swoole\Process;

class TimerProcess extends AbstractProcess
{

    public $interval = 1 * 1000; // 时间间隔

    /**
     * 此进程用来注册定时器
     */
    protected function run($arg)
    {
        $processName = $this->getProcessName();
        // $swooleProcess = $this->getProcess(); // 获取 注册进程的实例 \Swoole\Process
        $processPid = $this->getPid();
        $args = $this->getArg();
        \EasySwoole\Component\Timer::getInstance()->loop($this->interval, function () use ($args) {
            return self::test($args);
        });
        echo "### 注册 {$processName} 成功 [Pid: {$processPid}] ###\n";
    }

    public static function test($params)
    {
        
    }

    protected function onPipeReadable(Process $process)
    {
        // 该回调可选
        // 当主进程对子进程发送消息的时候 会触发
        $recvMsgFromMain = $process->read(); // 用于获取主进程给当前进程发送的消息
        var_dump('收到主进程发送的消息: ');
        var_dump($recvMsgFromMain);
    }

    protected function onException(\Throwable $throwable, ...$args)
    {
        // 该回调可选
        // 捕获 run 方法内抛出的异常
        // 这里可以通过记录异常信息来帮助更加方便地知道出现问题的代码
    }

    protected function onShutDown()
    {
        // 该回调可选
        // 进程意外退出 触发此回调
        // 大部分用于清理工作
    }

    protected function onSigTerm()
    {
        // 当进程接收到 SIGTERM 信号触发该回调
    }
}