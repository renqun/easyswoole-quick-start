<?php

namespace App\Process;

use EasySwoole\Component\Process\AbstractProcess;
use App\Queue\CancelOrderQueue;
use EasySwoole\Queue\Job;

/**
 * 取消订单消息队列 进程
 * @https://www.easyswoole.com/Components/Queue/queue.html#%E5%88%9B%E5%BB%BA%E9%98%9F%E5%88%97
 */
class CancelOrderQueueProcess extends AbstractProcess
{

    protected $desc = '处理 `取消订单` 消息队列中的任务';


    protected function run($arg)
    {
        go(function () {
            CancelOrderQueue::getInstance()->consumer()->listen(function (Job $job) {
                CancelOrderQueue::handle($job->getJobData());
            });
        });
        $processName = $this->getProcessName();
        $processPid  = $this->getPid();
        echo "### 注册 {$processName} 成功 [Pid: {$processPid}] ###\n";
        
    }

}
