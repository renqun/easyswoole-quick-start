<?php

namespace App\WebSocketController;

use EasySwoole\Socket\AbstractInterface\Controller;

class BaseSocketController extends Controller
{

    protected function success($data = [], $message = '')
    {
        $this->response()->setMessage(json_encode([
            'code'    => 200,
            'message' => $message,
            'data'    => $data
        ]));
    }


    protected function error($code = 500, $data = [], $message = 'error')
    {
        $this->response()->setMessage(json_encode([
            'code'    => $code,
            'message' => $message,
            'data'    => $data
        ]));
    }
}