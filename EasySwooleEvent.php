<?php


namespace EasySwoole\EasySwoole;

use App\Process\TimerProcess;
use App\Process\CancelOrderQueueProcess;
use App\Crontab\Kernel as CrontabKernel;
use App\Parser\WebSocketParser;
use App\Queue\CancelOrderQueue;
use EasySwoole\ORM\DbManager;
use EasySwoole\ORM\Db\Connection;
use EasySwoole\ORM\Db\Config as DbConfig;
use EasySwoole\Component\Di;
use EasySwoole\Redis\Config\RedisConfig;
use EasySwoole\Queue\Driver\RedisQueue;
use EasySwoole\Component\Process\Manager as ProcessManager;
use EasySwoole\Component\Process\Config as ProcessConfig;
use EasySwoole\EasySwoole\Config;
use EasySwoole\EasySwoole\SysConst;
use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\Http\Message\Status;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;

class EasySwooleEvent implements Event
{
    public static function initialize()
    {
        date_default_timezone_set('Asia/Shanghai');

        ###### 载入配置文件夹文件 ######
        // Config::getInstance()->loadDir(EASYSWOOLE_ROOT . '/Config', true);

        ###### 自定义异常处理 ######
        Di::getInstance()->set(SysConst::HTTP_EXCEPTION_HANDLER, [\App\Exception\Handler::class, 'render']);

        ###### 注册数据库连接池 ######
        $dbConfig = new DbConfig(Config::getInstance()->getConf("MYSQL"));
        DbManager::getInstance()->addConnection(new Connection($dbConfig));

        ###### 注册 Redis 连接池 ######
        new \App\Util\Redis\RedisPool();

        // 实现 onRequest 事件
        Di::getInstance()->set(SysConst::HTTP_GLOBAL_ON_REQUEST, function (Request $request, Response $response): bool {

            ###### 处理请求的跨域问题 ######
            $response->withHeader('Access-Control-Allow-Origin', '*');
            $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
            $response->withHeader('Access-Control-Allow-Credentials', 'true');
            $response->withHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
            if ($request->getMethod() === 'OPTIONS') {
                $response->withStatus(Status::CODE_OK);
                return false;
            }
            return true;
        });

        // 实现 afterRequest 事件
        Di::getInstance()->set(SysConst::HTTP_GLOBAL_AFTER_REQUEST, function (Request $request, Response $response): void {
            
        });
    }

    public static function mainServerCreate(EventRegister $register)
    {

        ###### Crontab任务计划 ######
        new CrontabKernel();

        $config = new \EasySwoole\Socket\Config();
        $config->setType($config::WEB_SOCKET);
        $config->setParser(WebSocketParser::class);
        $dispatcher = new \EasySwoole\Socket\Dispatcher($config);
        $config->setOnExceptionHandler(function (\Swoole\Server $server, \Throwable $throwable, string $raw, \EasySwoole\Socket\Client\WebSocket $client, \EasySwoole\Socket\Bean\Response $response) {
            var_dump($throwable->getMessage());
            $response->setMessage('system error!');
            $response->setStatus($response::STATUS_RESPONSE_AND_CLOSE);
        });

        ###### 注册定时器 ######
        ProcessManager::getInstance()->addProcess(new TimerProcess(new ProcessConfig([
            'processName'     => 'TimerProcess', // 设置 自定义进程名称
            'processGroup'    => 'Timer',        // 设置 自定义进程组名称
            'enableCoroutine' => true,          // 设置 自定义进程自动开启协程
        ])));

        ###### 配置 队列驱动器 ######
        $redisConfig = new RedisConfig(Config::getInstance()->getConf("REDIS"));
        $driver = new RedisQueue($redisConfig, 'queue');
        
        CancelOrderQueue::getInstance($driver);

        ###### 注册消费进程 CancelOrder ######
        ProcessManager::getInstance()->addProcess(new CancelOrderQueueProcess(new ProcessConfig([
            'processName'     => 'Queue.CancelOrder',
            'processGroup'    => 'Queue',
            'enableCoroutine' => true,
        ])));


        ###### ORM 连接预热 ######
        $register->add($register::onWorkerStart, function () {
            DbManager::getInstance()->getConnection()->__getClientPool()->keepMin();
        });


        // 自定义握手【设置 onHandShake 回调函数后不会再触发 onOpen 事件回调】
        /*$websocketEvent = new WebSocketEvent();
        $register->set(EventRegister::onHandShake, function (\Swoole\Http\Request $request, \Swoole\Http\Response $response) use ($websocketEvent) {
            $websocketEvent->onHandShake($request, $response);
        });*/

        // 处理 onOpen 回调
        $register->set($register::onOpen, function (\Swoole\WebSocket\Server $server, \Swoole\Http\Request $request) {
            $server->push($request->fd, "success\n");
        });


        // 处理 onMessage 回调
        $register->set($register::onMessage, function (\Swoole\Websocket\Server $server, \Swoole\Websocket\Frame $frame) use ($dispatcher) {
            $dispatcher->dispatch($server, $frame->data, $frame);
        });

        // 处理 onClose 回调
        $register->set($register::onClose, function ($ws, $fd) {
            echo "client-{$fd} is closed\n";
        });

        

    }

}